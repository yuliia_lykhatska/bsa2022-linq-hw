﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa2022_linq_hw.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Descriprion { get; set; }
        public int State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
    }
}
