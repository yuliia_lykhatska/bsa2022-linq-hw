﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa2022_linq_hw.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set;}
        public DateTime CreatedAt { get; set; } 
        public List<User> Members { get; set; }
    }
}
